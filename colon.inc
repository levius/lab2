%define colon_last 0

%macro colon 2
%2: 
    dq colon_last
    dq %%value
    db %1, 0
%%value:
%define colon_last %2
%endmacro